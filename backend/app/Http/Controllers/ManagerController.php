<?php

namespace App\Http\Controllers;

use App\Models\Manager;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    /**
     * Список всех менеджеров
     * @return array
     */
    public function getAll() : array
    {
        $data = Manager::all();

        return [
            'status'=>1,
            'data' => $data
        ];

    }

    /**
     * Добавить менеджера
     * @param Request $request
     * @return array
     */
    public function add(Request $request) : array
    {
        /*
        Валидаторы к сожалению не успеваю
        $request->validate([
            $request->json('name') => 'required|string',
            $request->json('email')=> 'email'
        ]);*/
            $manager = new Manager();

            $manager->name      = $request->json('name');
            $manager->email     = $request->json('email');
            $manager->phone     = $request->json('phone');
            $manager->end_date  = $request->json('end_date');
            $manager->status    = (int) $request->json('status');
            $manager->save();

            return ['status'=>1, 'data'=>'Данные сохранены'];
    }

    /**
     * Обновить менеджера
     * @param Request $request
     * @param         $id
     * @return array
     */
    public function update(Request $request, $id) : array
    {

        $manager = Manager::where(['id'=>$id])->first();

        if (!empty($manager))
        {
            $manager->name      = $request->json('name');
            $manager->email     = $request->json('email');
            $manager->phone     = $request->json('phone');
            $manager->end_date  = $request->json('end_date');
            $manager->status    = (int) $request->json('status');

            $manager->save();

            return ['status'=>1, 'data'=>'Данные сохранены'];

        }

        return ['status'=>0, 'text'=>'Несуществующий менеджер!'];
    }

    /**
     * Удалить менеджера
     * @param Request $request
     * @param         $id
     * @return array
     */
    public function delete(Request $request, $id) : array
    {
        $manager = Manager::where(['id'=>$id])->first();

        if (!empty($manager))
        {
            $manager->delete();
            return ['status'=>1, 'data'=>'Даннные удалены'];
        }
        return ['status'=>0, 'text'=>'Несуществующий менеджер!'];
    }
}
