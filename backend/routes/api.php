<?php

use App\Http\Controllers\ManagerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/



Route::prefix('managers')->group(function () {
    Route::get('/all',             [ManagerController::class, 'getAll']);
    Route::post('/add',            [ManagerController::class, 'add']);
    Route::post('/{id}/update',    [ManagerController::class, 'update']);
    Route::post('/{id}/delete',    [ManagerController::class, 'delete']);
});

