<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('managers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->datetime('end_date');
            $table->tinyInteger('status');
            $table->timestamps();
        });


        DB::table('managers')->insert([
            'name'      => "Rinat",
            'email'     =>'mail.rinat@yandex.ru',
            'phone'     => '+7(905)582-19-51',
            'status'    => 1,
            'end_date'  => '2025-01-01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('managers')->insert([
            'name'      => "Rinat2",
            'email'     =>'mail.rinat2@yandex.ru',
            'phone'     => '+7(905)582-19-52',
            'status'    => 1,
            'end_date'  => '2025-01-01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('managers')->insert([
            'name'      => "Rinat3",
            'email'     =>'mail.rinat@yandex.ru',
            'phone'     => '+7(905)582-19-54',
            'status'    => 1,
            'end_date'  => '2025-01-01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managers');

    }
}
