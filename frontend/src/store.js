import Vue from "vue";

export default Vue.observable({
    config: {
        api_url: 'http://localhost:8000/api',
    },
    managers: [],
    helpData: {
        status: ['Неактивен', 'Активен']
    }
});
